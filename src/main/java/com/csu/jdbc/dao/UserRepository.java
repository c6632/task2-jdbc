package com.csu.jdbc.dao;

import com.csu.jdbc.schema.User;

public interface UserRepository {
    String getUserById(Long id);
    String getUserByFirstName(String name);
    String getUserByLastName(String name);
    void save(User user);
}
